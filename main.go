package main

import (
	"flag"
	"fmt"
	"calculator/add"
)

func main() {
	var operand1, operand2, total int
	flag.IntVar(&operand1, "operand1", 1, "операнд 1")
	flag.IntVar(&operand2, "operand2", 1, "операнд 2")
	total = add.Add(operand1, operand2)
	fmt.Printf(" %d\n", total)
}
