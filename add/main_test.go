package add

import (
	"testing"
	"../../github.com/stretchr/testify/assert"
)

func Test_Add_PositiveInt_ReturnsSumOfBoth (t*testing.T){
	a, b, exp := 1, 1, 2
	assert.Equal(t, exp, Add(a, b))
}
